package it.uniroma3.progettosiw;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.uniroma3.progettosiw.model.Allievo;
import it.uniroma3.progettosiw.model.Attivita;
import it.uniroma3.progettosiw.model.Centro;
import it.uniroma3.progettosiw.service.AllievoService;
import it.uniroma3.progettosiw.service.AttivitaService;
import it.uniroma3.progettosiw.service.CentroService;

@SpringBootApplication
public class ProgettoSiwApplication {

	@Autowired
	private CentroService centroService; 

	@Autowired
	private AttivitaService attivitaService;

	@Autowired 
	private AllievoService allievoService;

	public static void main(String[] args) {
		SpringApplication.run(ProgettoSiwApplication.class, args);
	}

	@PostConstruct
	public void init() {



		Centro centro = new Centro("SwimClub", "Via della Vasca Navale", "Roma", "79", "SwimClub@roma.it", "3321459836",2);
		centroService.save(centro);
		Attivita attivita = new Attivita("Benvenuto", "22-6-18", "18:00");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Corso salvamento","23-06-18","14:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Scuola nuoto","23-06-18","16:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Nuoto sincronizzato","23-06-18","18:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Pallanuoto","23-06-18","19:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Master","23-06-18","20:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Pallanuoto","23-06-18","19:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Corso sub","23-06-18","21:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);


		centro = new Centro("DailySport", "via del corso", "Roma", "12", "DailySport@roma.it", "3377615339",20);
		centroService.save(centro);
		attivita = new Attivita("Benvenuto", "22-6-18", "18:00");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Scuola calcio","23-06-18","16:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Tennis","23-06-18","17:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Basket","23-06-18","18:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);


		attivita=new Attivita("Pallavolo","23-06-18","19:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Rugby","23-06-18","20:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);


		centro = new Centro("SportingClub", "Via Giacomo Grimaldi", "Roma", "5", "DailySport@roma.it", "3817296447",50);
		centroService.save(centro);
		attivita = new Attivita("Benvenuto", "22-6-18", "18:00");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Sala funzionale","23-06-18","14:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Sala pesi","23-06-18","16:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("CrossFit","23-06-18","18:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Danza","23-06-18","19:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Karate","23-06-18","20:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);



		centro = new Centro("RelaxingClub", "Via Alberto Manzi", "Roma", "42", "RelaxingClub@roma.it", "3475105837",25);
		centroService.save(centro);
		attivita = new Attivita("Benvenuto", "22/6/18", "18:00");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Yoga","23-06-18","14:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Massaggio Thailandese","23-06-18","16:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Sauna","23-06-18","18:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Posturale","23-06-18","19:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);

		attivita=new Attivita("Pilates","23-06-18","19:30");
		attivitaService.save(attivita);
		centro.addAttivita(attivita);
		centroService.save(centro);
		attivitaService.save(attivita);
	}

}
