package it.uniroma3.progettosiw.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import it.uniroma3.progettosiw.model.Allievo;
import it.uniroma3.progettosiw.model.Attivita;


public interface AllievoRepository extends CrudRepository<Allievo, Long> {

	public List<Allievo> findByNomeAndCognome(String nome, String cognome);

	public List<Allievo> findByNomeAndCognomeAndTelefono(String nome, String cognome, String telefono);

	public List<Allievo> findByAttivita(Attivita attivita);
}


