package it.uniroma3.progettosiw.repository;

import java.util.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

import it.uniroma3.progettosiw.model.Attivita;
import it.uniroma3.progettosiw.model.Centro;

public interface AttivitaRepository extends CrudRepository<Attivita, Long> {
	
	public List<Attivita> findByNome(String Nome);

	public List<Attivita> findByNomeAndDataAndOrarioAndCentro(String nome, String data, String orario, Centro centro);
	
	public List<Attivita> findByCentro(Centro centro);
	
	public List<Attivita> findByOrarioAndCentro(String orario, Centro centro);
	

}


