package it.uniroma3.progettosiw.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import it.uniroma3.progettosiw.model.Responsabile;

public interface ResponsabileRepository extends CrudRepository<Responsabile, Long> {
	
	public List<Responsabile> findByNomeAndMatricola(String nome, String Matricola);

	public List<Responsabile> findByNomeAndIdAndMatricola(String nome, Long id, String matricola);

	public List<Responsabile> findByMatricola(String matricola);
}















	
