package it.uniroma3.progettosiw.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

import it.uniroma3.progettosiw.model.Centro;

public interface CentroRepository  extends CrudRepository<Centro, Long>{
	
	public List<Centro> findByCittaAndIndirizzoAndNumeroCivico(String citta, String indirizzo, String numeroCivico);
	
	public List<Centro> findByNome(String nome);
	
}


