package it.uniroma3.progettosiw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.progettosiw.controller.validator.AllievoValidator;
import it.uniroma3.progettosiw.model.Allievo;
import it.uniroma3.progettosiw.model.Attivita;
import it.uniroma3.progettosiw.model.Centro;
import it.uniroma3.progettosiw.service.AllievoService;
import it.uniroma3.progettosiw.service.AttivitaService;
import it.uniroma3.progettosiw.service.CentroService;

@Controller
public class AllievoController{
	
	@Autowired
	private AllievoService allievoService;
	
	@Autowired
	AttivitaService attivitaService;
	
	@Autowired
	CentroService centroService;
	
	@Autowired
	private AllievoValidator validator;
	
	@RequestMapping("/centro/{idc}/attivita/{ida}/allievi")
	public String allievi(@PathVariable ("idc") Long idc, @PathVariable("ida") Long ida, Model model) {
		Centro centro = centroService.findById(idc);
		Attivita attivita = attivitaService.findById(ida);
		model.addAttribute("centro", centro);
		model.addAttribute("attivita", attivita);
		model.addAttribute("allievi", attivita.getAllievi());
		if(attivita.getAllievi().size()==0)
			model.addAttribute("listaVuota", "Nessun allievo è iscritto a questa attività");
		return "listaAllievi";
	}
	
	@RequestMapping("/centro/{idc}/attivita/{ida}/addAllievo")
		public String addAllievo(@PathVariable("idc") Long idc, @PathVariable("ida") Long ida, Model model) {
		Centro centro = centroService.findById(idc);
		Attivita attivita = attivitaService.findById(ida);
		model.addAttribute("centro", centro);
		model.addAttribute("attivita", attivita);
		model.addAttribute("allievo", new Allievo());
		return "allievoForm";
	}
	
	@RequestMapping(value = "/centro/{idc}/attivita/{ida}/allievo/{idal}", method = RequestMethod.GET)
	public String getAllievo(@PathVariable("idc") Long idc, @PathVariable("ida") Long ida, @PathVariable("idal") Long idal,
			Model model) {
		Centro centro = centroService.findById(idc);
		Attivita attivita = attivitaService.findById(ida);
		model.addAttribute("centro", centro);
		model.addAttribute("attivita", attivita);
		model.addAttribute("allievo", allievoService.findById(idal));
		return "mostraAllievo";
	}
	
	@RequestMapping(value = "/centro/{idc}/attivita/{ida}/allievo", method = RequestMethod.POST)
	public String newAllievo(@ModelAttribute("allievo") Allievo allievo,
			@PathVariable("idc") Long idc, @PathVariable("ida") Long ida,
			Model model, BindingResult bindingResult) {
		
		this.validator.validate(allievo, bindingResult);
		Centro centro = centroService.findById(idc);
		Attivita attivita = attivitaService.findById(ida);
		List<Allievo> allievi = attivita.getAllievi();
		
		model.addAttribute("centro", centro);
		model.addAttribute("attivita", attivita);
		
		if(allievi.contains(allievo)) {
			model.addAttribute("exists", "Questo Allievo già partecipa a questa Attività");
			return "allievoForm";
		}
		
		if(allievi.size() >= centro.getCapienza()) {
			model.addAttribute("exists", "Questa Attivita ha ragiunto la capienza massima");
			return "allievoForm";
		}
		
		else if(allievoService.alreadyExists(allievo)) {
			Allievo real = allievoService.findByNomeAndCognomeAndTelefono(allievo.getNome(), allievo.getCognome(), allievo.getTelefono());
			attivita.addAllievo(real);
			attivitaService.save(attivita);
			model.addAttribute("allievo", real);
			model.addAttribute("exists", "Allievo già registrato, ora iscritto a questa Attività");
			return "mostraAttivita";
		}
		
		else {
			if(!bindingResult.hasErrors()) {
				attivita.addAllievo(allievo);
				allievoService.save(allievo);
				attivitaService.save(attivita);
				return "mostraAttivita";
			}
			
			return "allievoForm";
		}
	}
	
	@RequestMapping(value = "/centro/{idc}/attivita/{ida}/allievo/{idal}", method = RequestMethod.POST)
	public String removeAllievo(@PathVariable("idc") Long idc, @PathVariable("ida") Long ida, @PathVariable("idal") Long idal,
			Model model) {
		Centro centro = centroService.findById(idc);
		Attivita attivita = attivitaService.findById(ida);
		Allievo allievo = allievoService.findById(idal);
		attivita.removeAllievo(allievo);
		attivitaService.save(attivita);
		allievoService.save(allievo);
		centroService.save(centro);
		model.addAttribute("centro", centro);
		model.addAttribute("attivita", attivita);
		return "mostraAttivita";
	}
	
	@RequestMapping("/azienda/allAllievi")
	public String allAllievi(Model model) {
		List<Allievo> allievi = allievoService.findAll();
		model.addAttribute("allievi", allievi);
		if(allievi.size()==0)
			model.addAttribute("listaVuota", "Nessun allievo è iscritto a questa attività");
		return "listaAllAllievi";
	}
	
	@RequestMapping("/azienda/allievo/{ida}")
	public String aziendaAttivita(@PathVariable("ida") Long ida, Model model) {
		Allievo allievo = allievoService.findById(ida);
		model.addAttribute("allievo", allievo);
		return "mostraAllievoAzienda";
	}
	
	@RequestMapping("/azienda/attivita/{ida}/allievi")
	public String attivitaAllievi(@PathVariable("ida") Long ida, Model model) {
		Attivita attivita = attivitaService.findById(ida);
		List<Allievo> allievi = attivita.getAllievi();
		model.addAttribute("allievi", allievi);
		if(allievi.size()==0)
			model.addAttribute("listaVuota", "Nessun allievo segue le attività");
		return "listaAllAllievi";
	}
}