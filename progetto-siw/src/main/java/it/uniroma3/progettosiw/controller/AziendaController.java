package it.uniroma3.progettosiw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import it.uniroma3.progettosiw.model.Allievo;
import it.uniroma3.progettosiw.model.Attivita;
import it.uniroma3.progettosiw.service.AllievoService;
import it.uniroma3.progettosiw.service.AttivitaService;
import it.uniroma3.progettosiw.service.CentroService;

@Controller
public class AziendaController {

	@Autowired
	private CentroService centroService;
	
	@Autowired
	private AttivitaService attivitaService;
	
	@Autowired
	private AllievoService allievoService;
	
	@RequestMapping("/azienda")
	public String azienda(Model model) {
		return "mostraAzienda";
	}
	
	@RequestMapping("/home")
	public String home(Model model) {
		return "index";
	}
	
	
}
