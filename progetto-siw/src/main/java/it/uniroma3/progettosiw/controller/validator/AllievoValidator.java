package it.uniroma3.progettosiw.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.progettosiw.model.Allievo;

@Component
public class AllievoValidator implements Validator {

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cognome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefono", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "luogoNascita", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "annoNascita", "required");
		
		InputValidator.rejectIfNotNumeric(errors, "telefono", "invalidInput", null, null);
		InputValidator.rejectIfNotEmailAddress(errors, "email", "invalidInput", null, null);
		//InputValidator.rejectIfNotData(errors, "annoNascita", "invalidInput", null, null);
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return Allievo.class.equals(aClass);
		
	}			
}











