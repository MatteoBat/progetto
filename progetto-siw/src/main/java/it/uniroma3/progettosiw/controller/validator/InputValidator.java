package it.uniroma3.progettosiw.controller.validator;

import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;

public class InputValidator {
	
	public static void rejectIfNumeric(
			Errors errors, String field, String errorCode, @Nullable Object[] errorArgs, @Nullable String defaultMessage) {

		Assert.notNull(errors, "Errors object must not be null");
		Object value = errors.getFieldValue(field);
		if (value == null ||isNumeric(value.toString())) {
			errors.rejectValue(field, errorCode, errorArgs, defaultMessage);
		}
	}
	
	public static void rejectIfNotNumeric(
			Errors errors, String field, String errorCode, @Nullable Object[] errorArgs, @Nullable String defaultMessage) {

		Assert.notNull(errors, "Errors object must not be null");
		Object value = errors.getFieldValue(field);
		if (value == null ||!isNumeric(value.toString())) {
			errors.rejectValue(field, errorCode, errorArgs, defaultMessage);
		}
	}
	
	public static void rejectIfContainsNumeric(
			Errors errors, String field, String errorCode, @Nullable Object[] errorArgs, @Nullable String defaultMessage) {

		Assert.notNull(errors, "Errors object must not be null");
		Object value = errors.getFieldValue(field);
		if (value == null ||containsNumeric(value.toString())) {
			errors.rejectValue(field, errorCode, errorArgs, defaultMessage);
		}
	}
	
	public static void rejectIfNotEmailAddress(
			Errors errors, String field, String errorCode, @Nullable Object[] errorArgs, @Nullable String defaultMessage) {

		Assert.notNull(errors, "Errors object must not be null");
		Object value = errors.getFieldValue(field);
		if (value == null ||!isEmailAddress(value.toString())) {
			errors.rejectValue(field, errorCode, errorArgs, defaultMessage);
		}
	}
    
	public static void rejectIfNotData(
			Errors errors, String field, String errorCode, @Nullable Object[] errorArgs, @Nullable String defaultMessage) {

		Assert.notNull(errors, "Errors object must not be null");
		Object value = errors.getFieldValue(field);
		if (value == null ||!isData(value.toString())) {
			errors.rejectValue(field, errorCode, errorArgs, defaultMessage);
		}
	}
	
	public static void rejectIfNotOrario(
			Errors errors, String field, String errorCode, @Nullable Object[] errorArgs, @Nullable String defaultMessage) {

		Assert.notNull(errors, "Errors object must not be null");
		Object value = errors.getFieldValue(field);
		if (value == null ||!isOrario(value.toString())) {
			errors.rejectValue(field, errorCode, errorArgs, defaultMessage);
		}
	}
	
    public static boolean isNumeric(String str) {
    	try 
    	{
    		Integer.parseInt(str);
    		return true;
    	}
    	catch (Exception e)
    	{
    	return false;
    	}
    }
    
    public static boolean containsNumeric(String str) {
    	return str.matches(".*[0-9].*");
    }
    
    public static boolean isEmailAddress(String str) {
    	return str.matches(".+@.*\\..+");
    }
    
    public static boolean isData(String str) {
    	return str.matches("[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]");
    }
    
    public static boolean isOrario(String str) {
    	return str.matches("[0-9][0-9]:[0-9][0-9]");
    }

}
