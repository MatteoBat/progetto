package it.uniroma3.progettosiw.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.progettosiw.model.Centro;

@Component
public class CentroValidator implements Validator {


	@Override
	public void validate(Object o, Errors errors) {
		/*Check Stringa nulla*/
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "indirizzo", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "citta", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "numeroCivico", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefono", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "capienza", "required");
		
		/*Check formato sbagliato: no-numerali
		
		InputValidator.rejectIfContainsNumeric(errors, "nome", "invalidFormat", null, null);
		InputValidator.rejectIfContainsNumeric(errors, "indirizzo", "invalidFormat", null, null);
		InputValidator.rejectIfContainsNumeric(errors, "citta", "invalidFormat", null, null);
		
		Check formato sbagliato: solo-numerali
		
		InputValidator.rejectIfNotNumeric(errors, "telefono", "invalidFormat", null, null);
		InputValidator.rejectIfNotNumeric(errors, "numeroCivico", "invalidFormat", null, null);
		InputValidator.rejectIfNotNumeric(errors, "capienza", "invalidFormat", null, null);
		
		Check formato sbagliato: indirizzo-email
		
		InputValidator.rejectIfNotEmailAddress(errors, "email", "invalidFormat", null, null);
		*/
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return Centro.class.equals(aClass);

	}			
}

























