package it.uniroma3.progettosiw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.progettosiw.controller.validator.AttivitaValidator;
import it.uniroma3.progettosiw.model.Allievo;
import it.uniroma3.progettosiw.model.Attivita;
import it.uniroma3.progettosiw.model.Centro;
import it.uniroma3.progettosiw.service.AllievoService;
import it.uniroma3.progettosiw.service.AttivitaService;
import it.uniroma3.progettosiw.service.CentroService;

@Controller
public class AttivitaController {

	@Autowired
	private AttivitaService attivitaService;

	@Autowired
	private CentroService centroService;
	
	@Autowired
	private AllievoService allievoService;

	@Autowired
	private AttivitaValidator validator;

	@RequestMapping("/centro/{id}/attivitas")
	public String attivitas(@PathVariable("id") Long id, Model model) {
		Centro centro = centroService.findById(id);
		List<Attivita> attivitas = centro.getAttivita();
		model.addAttribute("centro", centro);
		model.addAttribute("attivitas", attivitas);
		if (attivitas.size() == 0)
			model.addAttribute("listaVuota", "Questo Centro non ha ancora un'attività");
		return "listaAttivita";
	}
	
	@RequestMapping("/centro/{id}/addAttivita")
	public String addAttivita(@PathVariable("id") Long id, Model model) {
		Centro centro = centroService.findById(id);
		model.addAttribute("centro", centro);
		model.addAttribute("attivita", new Attivita());
		return "attivitaForm";
	}

	@RequestMapping("/centro/{idc}/attivita/{ida}")
	public String attivita(@PathVariable("idc") Long idc, @PathVariable("ida") Long ida, Model model) {
		Centro centro = centroService.findById(idc);
		Attivita attivita = attivitaService.findById(ida);
		model.addAttribute("centro", centro);
		model.addAttribute("attivita", attivita);
		return "mostraAttivita";
	}

	@RequestMapping(value = "/centro/{idc}/attivita", method = RequestMethod.POST)
	public String newAttivita(@ModelAttribute("attivita") Attivita attivita,
			@PathVariable("idc") Long idc, Model model, BindingResult bindingResult) {

		this.validator.validate(attivita, bindingResult);
		Centro centro = centroService.findById(idc);
		attivita.setCentro(centro);
		List<Attivita> attivitas = centro.getAttivita();
		
		model.addAttribute("centro", centro);

		if(attivitas.contains(attivita)) {
			model.addAttribute("centro", centroService.findById(idc));
			model.addAttribute("exists", "Questa attività già esiste in questo centro");
			return "attivitaForm";
		}

		else if(centro.orarioOccupato(attivita)) {
			model.addAttribute("centro", centroService.findById(idc));
			model.addAttribute("exists", "Attività già esistente in questa fascia oraria");
			return "attivitaForm";
		}

		else {
			if(!bindingResult.hasErrors()) {
				attivitaService.save(attivita);
				centro.addAttivita(attivita);
				attivitaService.save(attivita);
				centroService.save(centro);
				model.addAttribute("centro", centroService.findById(idc));
				model.addAttribute("attivitas", centro.getAttivita());
				return "listaAttivita";
			}

			return "attivitaForm";
		}
	}

	@RequestMapping(value = "/centro/{idc}/attivita/{ida}", method = RequestMethod.POST)
	public String removeAttivita(@PathVariable("idc") Long idc, @PathVariable("ida") Long ida,
			Model model) {
		Centro centro = centroService.findById(idc);
		Attivita attivita = attivitaService.findById(ida);
		List<Allievo> allievi = attivita.getAllievi();
		centro.removeAttivita(attivita);
		for(Allievo a : allievi) {
			allievoService.save(a);
		}
		centroService.save(centro);
		attivitaService.delete(attivita);
		model.addAttribute("centro", centro);
		List<Attivita> attivitas = centro.getAttivita();
		model.addAttribute("attivitas", attivitas);
		if (attivitas.size() == 0)
			model.addAttribute("listaVuota", "Questo Centro non ha ancora un'attivita");
		return "mostraCentro";

	}
	
	@RequestMapping("/azienda/allAttivitas")
	public String allAllievi(Model model) {
		List<Attivita> attivitas = attivitaService.findAll();
		model.addAttribute("attivitas", attivitas);
		if(attivitas.size()==0)
			model.addAttribute("attivitaVuota", "Non ci sono attività in corso");
		return "listaAllAttivitas";
	}
	
	@RequestMapping("/azienda/attivita/{ida}")
	public String aziendaAttivita(@PathVariable("ida") Long ida, Model model) {
		Attivita attivita = attivitaService.findById(ida);
		model.addAttribute("attivita", attivita);
		return "mostraAttivitaAzienda";
	}
	
	@RequestMapping("/azienda/allievo/{ida}/attivitas")
	public String allieviAttivita(@PathVariable("ida") Long ida, Model model) {
		Allievo allievo = allievoService.findById(ida);
		List<Attivita> attivitas = allievo.getAttivita();
		model.addAttribute("attivitas", attivitas);
		if(attivitas.size()==0)
			model.addAttribute("attivitaVuota", "Non ci sono attività in corso");
		return "listaAllAttivitas";
	}
}
