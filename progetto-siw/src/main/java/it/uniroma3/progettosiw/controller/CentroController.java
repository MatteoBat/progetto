package it.uniroma3.progettosiw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.progettosiw.controller.validator.CentroValidator;
import it.uniroma3.progettosiw.service.CentroService;

@Controller
public class CentroController {

	@Autowired
	private CentroService centroService;
	
	@Autowired
	private CentroValidator validator;
	
	@RequestMapping("/centri")
	public String centri(Model model) {
		model.addAttribute("centri", this.centroService.findAll());
		return "listaCentri";
	}
	
	@RequestMapping(value = "/centro/{id}", method = RequestMethod.GET)
	public String getCentro(@PathVariable("id") Long id, Model model) {
		model.addAttribute("centro", centroService.findById(id));
		return "mostraCentro";
	}
	
}
