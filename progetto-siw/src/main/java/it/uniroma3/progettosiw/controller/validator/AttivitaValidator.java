package it.uniroma3.progettosiw.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.progettosiw.model.Attivita;

@Component
public class AttivitaValidator implements Validator {

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "data", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "orario", "required");
		
		//InputValidator.rejectIfNotData(errors, "data", "invalidInput", null, null);
		//InputValidator.rejectIfNotOrario(errors, "orario", "invalidInput", null, null);
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return Attivita.class.equals(aClass);

	}			
}























