package it.uniroma3.progettosiw.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.progettosiw.model.Attivita;
import it.uniroma3.progettosiw.model.Centro;
import it.uniroma3.progettosiw.repository.AttivitaRepository;

@Transactional
@Service
public class AttivitaService {
	
	@Autowired
	private AttivitaRepository attivitaRepository;
	
	public Attivita save(Attivita attivita) {
		return this.attivitaRepository.save(attivita);
	}
	
	public List<Attivita> findByNome(String nome){
		return this.attivitaRepository.findByNome(nome);
	}
	
	public List<Attivita> findAll(){
		return (List<Attivita>)this.attivitaRepository.findAll();
	}
	
	public Attivita findById(Long id) {
		Optional<Attivita> attivita = this.attivitaRepository.findById(id);
		if (attivita.isPresent())
			return attivita.get();
		else
			return null;
	}
	
	public boolean alreadyExists(Attivita attivita) {
		List<Attivita> attivitas = this.attivitaRepository.findByNomeAndDataAndOrarioAndCentro(attivita.getNome(), attivita.getData(), attivita.getOrario(), attivita.getCentro());
		if (attivitas.size() > 0)
			return true;
		else return false;
	}

	public List<Attivita> findByCentro(Centro centro){
		return this.attivitaRepository.findByCentro(centro);
	}
	public List<Attivita> findByNomeAndDataAndOrarioAndCentro(String nome, String data, String orario, Centro centro) {
		
		return attivitaRepository.findByNomeAndDataAndOrarioAndCentro(nome, data, orario, centro);
	}
	
	public void delete(Attivita attivita) {
		attivitaRepository.delete(attivita);
	}
}
