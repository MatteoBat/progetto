package it.uniroma3.progettosiw.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.progettosiw.model.Allievo;
import it.uniroma3.progettosiw.model.Attivita;
import it.uniroma3.progettosiw.repository.AllievoRepository;

@Transactional
@Service
public class AllievoService {
	
	@Autowired
	private AllievoRepository allievoRepository; 
	
	public Allievo save(Allievo allievo) {
		return this.allievoRepository.save(allievo);
	}

	public List<Allievo> findByNomeAndCognome(String nome, String cognome) {
		return this.allievoRepository.findByNomeAndCognome(nome, cognome);
	}

	public List<Allievo> findByAttivita(Attivita attivita){
		return this.allievoRepository.findByAttivita(attivita);
	}
	
	public List<Allievo> findAll() {
		return (List<Allievo>) this.allievoRepository.findAll();
	}
	
	public Allievo findById(Long id) {
		Optional<Allievo> Allievo = this.allievoRepository.findById(id);
		if (Allievo.isPresent()) 
			return Allievo.get();
		else
			return null;
	}
	
	public Allievo findByNomeAndCognomeAndTelefono(String nome, String cognome, String telefono) {
		return this.allievoRepository.findByNomeAndCognomeAndTelefono(nome, cognome, telefono).get(0);
	}

	public boolean alreadyExists(Allievo allievo) {
		List<Allievo> Allievo = this.allievoRepository.findByNomeAndCognomeAndTelefono(allievo.getNome(), allievo.getCognome(), allievo.getTelefono());
		if (Allievo.size() > 0)
			return true;
		else 
			return false;
	}

	public void delete(Allievo allievo) {
		allievoRepository.delete(allievo);
	}	
}












	

