package it.uniroma3.progettosiw.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.progettosiw.model.Responsabile;
import it.uniroma3.progettosiw.repository.ResponsabileRepository;

@Transactional
@Service
public class ResponsabileService {
	@Autowired
	private ResponsabileRepository ResponsabileRepository; 
	
	public Responsabile save(Responsabile responsabile) {
		return this.ResponsabileRepository.save(responsabile);
	}

	public Optional<Responsabile> findByid(Long id) {
		return this.ResponsabileRepository.findById(id);
	}
	
	public List<Responsabile> findByMatricola(String matricola){
		return this.ResponsabileRepository.findByMatricola(matricola);
	}

	public List<Responsabile> findAll() {
		return (List<Responsabile>) this.ResponsabileRepository.findAll();
	}
	
	public Responsabile findById(Long id) {
		Optional<Responsabile> Responsabile = this.ResponsabileRepository.findById(id);
		if (Responsabile.isPresent()) 
			return Responsabile.get();
		else
			return null;
	}

	public boolean alreadyExists(Responsabile responsabile) {
		List<Responsabile> Responsabile = this.ResponsabileRepository.findByNomeAndIdAndMatricola(responsabile.getNome(), responsabile.getId(), responsabile.getMatricola());
		if (Responsabile.size() > 0)
			return true;
		else 
			return false;
	}	
}












	

