package it.uniroma3.progettosiw.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.lang.String;
import java.lang.Object;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Attivita {
	
	public Attivita() {
		this.allievi = new ArrayList<Allievo>();
	}
	
	public Attivita(String nome, String data, String orario) {
		super();
		this.nome = nome;
		this.data = data;
		this.orario = orario;
		this.allievi = new ArrayList<Allievo>();
	}
	

	public Attivita(String nome, String data, String orario, Centro centro) {
		super();
		this.nome = nome;
		this.data = data;
		this.orario = orario;
		this.centro = centro;
		this.allievi = new ArrayList<Allievo>();
	}


	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	@Column(nullable = false)
	private String data;
	
	@Column(nullable = false)
	private String orario;
	
	@ManyToMany
	private List<Allievo> allievi;
	
	@ManyToOne
	private Centro centro;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getOrario() {
		return orario;
	}

	public void setOrario(String orario) {
		this.orario = orario;
	}

	public List<Allievo> getAllievi() {
		return allievi;
	}

	public void setAllievi(List<Allievo> allievi) {
		this.allievi = allievi;
	}

	public Centro getCentro() {
		return centro;
	}

	public void setCentro(Centro centro) {
		this.centro = centro;
	}
	
	public void addAllievo(Allievo allievo) {
		allievi.add(allievo);
		allievo.getAttivita().add(this);
	}
	
	public void removeAllievo(Allievo allievo) {
		allievi.remove(allievo);
		allievo.getAttivita().remove(this);
	}

	@Override
	public boolean equals(Object o) {
		Attivita that = (Attivita)o;
		return this.nome.equals(that.getNome()) && this.data.equals(that.getData()) && this.orario.equals(that.getOrario()) && this.centro.equals(that.getCentro());
	}
	
	@Override
	public int hashCode() {
		return this.nome.hashCode() + this.data.hashCode() + this.orario.hashCode() + this.centro.hashCode();
	}
}
