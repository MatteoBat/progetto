package it.uniroma3.progettosiw.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.lang.String;
import java.lang.Long;
import java.lang.Object;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Value;

@Entity
public class Allievo {
	
	public Allievo() {
		this.attivita = new ArrayList<Attivita>();
	}

	public Allievo(String nome, String cognome, String email, String luogoNascita, String telefono,
			String annoNascita) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
		this.luogoNascita = luogoNascita;
		this.telefono = telefono;
		this.annoNascita = annoNascita;
		this.attivita = new ArrayList<Attivita>();
	}

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	@Column(nullable=false)
	private String cognome;
	
	@Column(nullable=false)
	private String email;
	
	@Column(nullable=false)
	private String luogoNascita;
	
	@Column(nullable=false)
	private String telefono;
	
	@Column(nullable=false)
	private String annoNascita;
	
	@ManyToMany(mappedBy="allievi")
	private List<Attivita> attivita;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getLuogoNascita() {
		return luogoNascita;
	}

	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getAnnoNascita() {
		return annoNascita;
	}

	public void setAnnoNascita(String annoNascita) {
		this.annoNascita = annoNascita;
	}

	public List<Attivita> getAttivita() {
		return attivita;
	}

	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}

	
	@Override
	public boolean equals(Object o) {
		Allievo that = (Allievo)o;
		return this.nome.equals(that.getNome()) && this.cognome.equals(that.getCognome())
				&& this.telefono.equals(that.getTelefono());
	}
	
	@Override
	public int hashCode() {
		return this.nome.hashCode() + this.cognome.hashCode()
		+ this.telefono.hashCode();	
	}
	
}
