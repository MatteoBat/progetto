package it.uniroma3.progettosiw.model;

import java.util.ArrayList;
import java.util.List;
import java.lang.String;
import java.lang.Long;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Centro {
	
	public Centro() {
		this.attivitas = new ArrayList<Attivita>();
	}

	public Centro(String nome, Azienda azienda, String indirizzo, String citta, String numeroCivico, String email, String telefono,
			Integer capienza) {
		super();
		this.nome = nome;
		this.azienda = azienda;
		this.indirizzo = indirizzo;
		this.citta = citta;
		this.numeroCivico = numeroCivico;
		this.email = email;
		this.telefono = telefono;
		this.capienza = capienza;
		this.attivitas = new ArrayList<Attivita>();
	}

	public Centro(String nome, String indirizzo, String citta, String numeroCivico, String email, String telefono,
			Integer capienza) {
		super();
		this.nome = nome;
		this.indirizzo = indirizzo;
		this.citta = citta;
		this.numeroCivico = numeroCivico;
		this.email = email;
		this.telefono = telefono;
		this.capienza = capienza;
		this.attivitas = new ArrayList<Attivita>();
	}


	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
		
	@Column(nullable=false)
	private String indirizzo;
	
	@Column(nullable=false)
	private String citta;
	
	@Column(nullable=false)
	private String numeroCivico;
	
	@Column(nullable=false)
	private String email;
	
	@Column(nullable=false)
	private String telefono;
	
	@Column(nullable=false)
	private Integer capienza;
	
	@OneToMany(mappedBy="centro")
	private List<Attivita> attivitas;
	
	@ManyToOne
	private Azienda azienda;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getNumeroCivico() {
		return numeroCivico;
	}

	public void setNumeroCivico(String numeroCivico) {
		this.numeroCivico = numeroCivico;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Integer getCapienza() {
		return capienza;
	}

	public void setCapienza(Integer capienza) {
		this.capienza = capienza;
	}

	public List<Attivita> getAttivita() {
		return attivitas;
	}

	public void setAttivita(List<Attivita> attivita) {
		this.attivitas = attivita;
	}

	public Azienda getAzienda() {
		return azienda;
	}

	public void setAzienda(Azienda azienda) {
		this.azienda = azienda;
	}
	public void removeAttivita(Attivita attivita) {
		List <Allievo> allievi= attivita.getAllievi();
		for(Allievo allievo: allievi) {
			allievo.getAttivita().remove(attivita);
		}
			
		attivitas.remove(attivita);
	}
	public void addAttivita(Attivita attivita) {
		attivitas.add(attivita);
		attivita.setCentro(this);
	}
	
	public boolean orarioOccupato(Attivita attivita) {
		for (Attivita att : attivitas) {
			if (attivita.getData().equals(att.getData()) && attivita.getOrario().equals(att.getOrario()))
				return true;
		}
		return false;
	}
	
	@Override
	public boolean equals(Object o) {
		Centro that = (Centro)o;
		return this.nome.equals(that.getNome()) && this.telefono.equals(that.getTelefono());
	}
	
	@Override
	public int hashCode() {
		return this.nome.hashCode() + this.telefono.hashCode();
	}
}