package it.uniroma3.progettosiw.model;

import java.util.Date;
import java.util.List;
import java.lang.String;
import java.lang.Long;
import java.lang.Object;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Responsabile {
	
	public Responsabile() {
		super();
	}
	public Responsabile(String nome, String matricola ) {
		super();
		this.nome = nome;
		this.matricola = matricola;
	}
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	@Column(nullable=false)
	private String matricola;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMatricola() {
		return matricola;
	}

	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}	
	
	@Override
	public boolean equals(Object o) {
		Responsabile that = (Responsabile)o;
		return this.nome.equals(that.nome) && this.matricola.equals(that.matricola);
	}
	
	@Override
	public int hashCode() {
		return this.nome.hashCode() + this.matricola.hashCode();
	}


}
